FROM microsoft/aspnetcore
WORKDIR /app
COPY . .
EXPOSE 80
ENTRYPOINT ["dotnet", "aspnet-core-pipelines.dll"]